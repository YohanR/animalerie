import * as exercices1 from './exercice1.js'
exercices1.exercice1()
import * as exercices2 from './exercice2.js'
exercices2.exercice2()
import * as exercices3 from './exercice3.js'
exercices3.exercice3()
import * as exercices4 from './exercice4.js'
exercices4.exercice4()
import * as exercicesbonus from './bonus.js'
exercicesbonus.bonus()